package br.com.diegosabino.jpatesting.config;

import br.com.diegosabino.jpatesting.entity.User;
import br.com.diegosabino.jpatesting.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class DataInitializr implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    UserRepository userRepository;


    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        List<User> users = userRepository.findAll();

        if (users.isEmpty()) {
            createUser("Diego Sabino", "dgrego.9@gmail.com");
            createUser("João", "joao@gmail.com");
            createUser("Maria", "maria@gmail.com");
        }


    }

    public void createUser(String name, String email) {
        User user = new User(name, email);
        userRepository.save(user);
    }
}
