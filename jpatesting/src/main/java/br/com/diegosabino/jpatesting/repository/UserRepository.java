package br.com.diegosabino.jpatesting.repository;

import br.com.diegosabino.jpatesting.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, Long> {

}
